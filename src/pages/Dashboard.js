import { Helmet } from 'react-helmet';
import { useState } from 'react';
import {
  Box, Container, Grid, IconButton
} from '@material-ui/core';
import Budget from 'src/components/dashboard//Budget';
import LatestOrders from 'src/components/dashboard//LatestOrders';
// import LatestProducts from 'src/components/dashboard//LatestProducts';
import UserCard from 'src/components/dashboard//Card';
import Sales from 'src/components/dashboard//Sales';
import TasksProgress from 'src/components/dashboard//TasksProgress';
import TotalCustomers from 'src/components/dashboard//TotalCustomers';
import TotalProfit from 'src/components/dashboard//TotalProfit';
import TrafficByDevice from 'src/components/dashboard//TrafficByDevice';
import moment from 'moment';
import CustomerList from './CustomerList';

const Dashboard = () => {
  const [showLoan, setShowLoan] = useState(false);
  return (
    <>
      <Helmet>
        <title>Dashboard | Material Kit</title>
      </Helmet>
      {!showLoan ? (
        <Box
          sx={{
            backgroundColor: 'background.default',
            minHeight: '100%',
            py: 3
          }}
        >
          <h2 style={{ padding: '15px 25px', fontFamily: 'Roboto' }}>
            Dashboard {' '}
            <span style={{ fontSize: 12, color: 'blue' }}>
              {moment().format('dddd ll')}
            </span>
            <IconButton
              color="inherit"
              style={{ float: 'right', cursor: 'pointer', fontSize: 14 }}
              onClick={() => setShowLoan(true)}
            >
              Add New Loan
            </IconButton>
          </h2>

          <Container maxWidth={false}>
            <Grid container spacing={3}>
              <Grid item lg={3} sm={6} xl={3} xs={12}>
                <Budget />
              </Grid>
              <Grid item lg={3} sm={6} xl={3} xs={12}>
                <TotalCustomers />
              </Grid>
              <Grid item lg={3} sm={6} xl={3} xs={12}>
                <TasksProgress />
              </Grid>
              <Grid item lg={3} sm={6} xl={3} xs={12}>
                <TotalProfit sx={{ height: '100%' }} />
              </Grid>
              <Grid item lg={4} md={12} xl={9} xs={12}>
                <UserCard />
              </Grid>
              <Grid item lg={4} md={12} xl={9} xs={12}>
                <Sales />
              </Grid>
              <Grid item lg={4} md={6} xl={3} xs={12}>
                <TrafficByDevice sx={{ height: '100%' }} />
              </Grid>
              <Grid item lg={12} md={12} xl={12} xs={12}>
                <LatestOrders />
              </Grid>
            </Grid>
          </Container>
        </Box>
      ) : (
        <CustomerList />
      )}
    </>
  );
};

export default Dashboard;
