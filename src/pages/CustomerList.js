import { useState } from "react";
import {
  Container,
  Typography,
  Box,
  Switch,
  Grid,
  TextField,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import moment from "moment";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { DropzoneArea } from "material-ui-dropzone";

const useStyles = makeStyles((theme) => ({
  formCont: {
    margin: "15px 0",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: 150,
  },
  cancelBtn: {
    background: "transparent !important",
    color: "#71D41A !important",
    border: "1px solid #71D41A !important",
    marginLeft: "20px !important",
  },
}));

const CustomerList = () => {
  const classes = useStyles();
  const [state, setState] = useState({
    checkedA: true,
    checkedB: true,
  });
  const [age, setAge] = useState("");
  const [files, setFiles] = useState(null);

  const handleChanges = (event) => {
    setAge(event.target.value);
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleChangeDrop = (file) => {
    setFiles(file);
  };
  return (
    <>
      <Container maxWidth={false}>
        <h2 style={{ padding: "15px 0", fontFamily: "Roboto" }}>
          Add Loan Officer{" "}
          <span style={{ fontSize: 12, color: "blue" }}>
            {moment().format("dddd ll")}
          </span>
        </h2>
        <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: 12 }}>
          <Link color="#71D41A" href="/">
            Home
          </Link>
          <Link color="#71D41A" href="/getting-started/installation/">
            Manage Loan Officer
          </Link>
          <Typography color="textPrimary">Add Loan Officer</Typography>
        </Breadcrumbs>
        <Box sx={{ maxWidth: 900 }}>
          <form className={classes.formCont}>
            <div>
              <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">Role</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={age}
                  onChange={handleChanges}
                >
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>
              <span style={{ float: "right" }}>
                <Switch
                  checked={state.checkedB}
                  onChange={handleChange}
                  color="primary"
                  name="checkedB"
                  inputProps={{ "aria-label": "primary checkbox" }}
                />
              </span>
            </div>
            <Grid container spacing={3}>
              <Grid item xs={3}>
                <FormControl className={classes.formControl}>
                  <InputLabel id="demo-simple-select-label">
                    Salutation
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={age}
                    onChange={handleChanges}
                  >
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="First Name"
                  required
                />
              </Grid>
              <Grid item xs={3} />
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Middle Name"
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Last Name"
                  required
                />
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Mobile Number"
                  type="number"
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Email Address"
                  type="email"
                />
              </Grid>
            </Grid>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Admin ID"
                  required
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="standard-basic"
                  style={{ width: "100%" }}
                  label="Admin Password"
                  type="password"
                  required
                />
              </Grid>
            </Grid>
            <div style={{ margin: "10px 0" }}>
              <h5 style={{ margin: "10px 0" }}>Upload Image:</h5>
              <DropzoneArea onChange={handleChangeDrop} />
              <p>
                <i>
                  Note: Only JPEG, JPG or PNG files. Max file size - 1MB. Size -
                  100x100 px
                </i>
              </p>
            </div>
            <Box style={{ margin: "30px 10px" }}>
              <Button variant="contained" disabled>
                Save
              </Button>
              <Button variant="contained" className={classes.cancelBtn}>
                Cancel
              </Button>
            </Box>
          </form>
        </Box>
      </Container>
    </>
  );
};
export default CustomerList;
