import {
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';

const TotalProfit = (props) => (
  <Card {...props}>
    <CardContent>
      <Grid container spacing={3} sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography color="textSecondary" gutterBottom variant="h6">
            Yearly Sales
          </Typography>
          <Typography className="chip-card" color="textPrimary" variant="h3">
            $4 million
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);

export default TotalProfit;
