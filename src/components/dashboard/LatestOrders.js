import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
    border: 'none'
  },
  margin: {
    margin: theme.spacing(1),
    width: 400
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    width: 200
  }
}));

function createData(
  status,
  dateOfRequest,
  loanOfficer,
  consumerName,
  suffix,
  consumerEmailId,
  consumer,
  products,
  consumerDesign,
  mlServiceId,
  actions
) {
  return {
    status,
    dateOfRequest,
    loanOfficer,
    consumerName,
    suffix,
    consumerEmailId,
    consumer,
    products,
    consumerDesign,
    mlServiceId,
    actions
  };
}

const rows = [
  createData(
    'green',
    '11/02/2020',
    'Alex T',
    'Thomas',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Alma C',
    'Campbell',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'John T',
    'Trevor',
    'Jr',
    'som3samplemail@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Abandon',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Alex T',
    'Thomas',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Alex T',
    'Thomas',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Kate',
    'Johnson',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'red',
    '11/02/2020',
    'Alma C',
    'Campbell',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'red',
    '11/02/2020',
    'DeanT',
    'Lenno',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Alex T',
    'Thomas',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  ),
  createData(
    'green',
    '11/02/2020',
    'Alma',
    'Campbell',
    'Jr',
    'cambell.joe2298@example.com',
    '+1(202)555-0123',
    '4.56% for 30 years',
    'Yes',
    924899,
    '.'
  )
];

export default function LatestOrders() {
  const classes = useStyles();
  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <div>
      <TextField
        id="date"
        label="Date Picker"
        type="date"
        defaultValue="2017-05-24"
        className={classes.textField}
        InputLabelProps={{
          shrink: true
        }}
      />
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">Dropdown</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          onChange={handleChange}
        >
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
      </FormControl>
      <TextField
        className={classes.margin}
        id="input-with-icon-textfield"
        label="Search"
        InputProps={{
          startAdornment: (
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          )
        }}
      />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>STATUS</TableCell>
              <TableCell>DATE OF REQUEST</TableCell>
              <TableCell>LOAN OFFICER</TableCell>
              <TableCell>CONSUMER NAME</TableCell>
              <TableCell>SUFFIX</TableCell>
              <TableCell>CONSUMER EMAIL ID</TableCell>
              <TableCell>CONSUMER</TableCell>
              <TableCell>PRODUCTS</TableCell>
              <TableCell>CONSUMER DESIGN</TableCell>
              <TableCell>ML SERVICE ID</TableCell>
              <TableCell>ACTIONS </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.status}>
                <TableCell component="th" scope="row">
                  <span style={{ color: row.status, fontSize: 30 }}>
                    &#9679;
                  </span>
                </TableCell>
                <TableCell align="right">{row.dateOfRequest}</TableCell>
                <TableCell align="right">{row.loanOfficer}</TableCell>
                <TableCell align="right">{row.consumerName}</TableCell>
                <TableCell align="right">{row.suffix}</TableCell>
                <TableCell align="right">{row.consumerEmailId}</TableCell>
                <TableCell align="right">{row.consumer}</TableCell>
                <TableCell align="right">{row.products}</TableCell>
                <TableCell align="right">{row.consumerDesign}</TableCell>
                <TableCell align="right">{row.mlServiceId}</TableCell>
                <TableCell align="right">{row.actions}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
