import {
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';

const TasksProgress = (props) => (
  <Card {...props}>
    <CardContent>
      <Grid container spacing={3} sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography color="textSecondary" gutterBottom variant="h6">
            Loans Rejected
          </Typography>
          <Typography className="chip-card" color="textPrimary" variant="h3">
            1,235
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);

export default TasksProgress;
