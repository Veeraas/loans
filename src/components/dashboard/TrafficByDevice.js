import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts';
import {
  Box, Card, CardContent, CardHeader
} from '@material-ui/core';

const data = [
  {
    name: 'Page B',
    refund: 0,
    auth: 1398,
    sale: 2210
  },
  {
    name: 'Page C',
    refund: 1,
    auth: 9800,
    sale: 2290
  },
  {
    name: 'Page D',
    refund: 2,
    auth: 3908,
    sale: 2000
  },
  {
    name: 'Page E',
    refund: 3,
    auth: 4800,
    sale: 2181
  },
  {
    name: 'Page F',
    refund: 4,
    auth: 3800,
    sale: 2500
  },
  {
    name: 'Page G',
    refund: 5,
    auth: 4300,
    sale: 2100
  }
];

export default function TrafficByDevice() {
  return (
    <Card>
      <CardHeader title="Bar Chart" />
      <CardContent>
        {' '}
        <Box
          sx={{
            height: 220,
            position: 'relative'
          }}
        >
          <ResponsiveContainer width="100%" height="100%">
            <BarChart
              width={730}
              height={200}
              data={data}
              layout="vertical"
              barSize={20}
            >
              <CartesianGrid strokeDasharray="6 6" />
              <XAxis />
              <YAxis dataKey="refund" />
              <Tooltip />
              <Legend />
              <Bar dataKey="auth" fill="orange" />
              <Bar dataKey="refund" fill="pink" />
              <Bar dataKey="sale" fill="blue" />
            </BarChart>
          </ResponsiveContainer>
        </Box>
      </CardContent>
    </Card>
  );
}
