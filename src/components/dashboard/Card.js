import { Card, CardContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  cont: {
    background: '#5664D2 !important',
    color: '#fff !important'
  },
  title: {
    borderBottom: '1px solid #f5f5f5'
  },
  details: {
    padding: '0 6px',
    paddingTop: 27,
    borderBottom: '1px solid #fff'
  },
  count: {
    float: 'right'
  }
}));

export default function UserCard() {
  const classes = useStyles();
  return (
    <div>
      <Card className={classes.cont}>
        <CardContent>
          <h3 className={classes.title}>card Title</h3>
          <h3 className={classes.details}>
            User Online
            <span className={classes.count}>500</span>
          </h3>
          <h3 className={classes.details}>
            User Online
            <span className={classes.count}>500</span>
          </h3>
          <h3 className={classes.details}>
            User Online
            <span className={classes.count}>500</span>
          </h3>
          <h3 className={classes.details}>
            User Online
            <span className={classes.count}>500</span>
          </h3>
          <h3 className={classes.details}>
            User Online
            <span className={classes.count}>500</span>
          </h3>
        </CardContent>
      </Card>
    </div>
  );
}
