import {
  Card, CardContent, Grid, Typography
} from '@material-ui/core';

const Budget = (props) => (
  <Card {...props}>
    <CardContent>
      <Grid container spacing={3} sx={{ justifyContent: 'space-between' }}>
        <Grid item>
          <Typography color="textSecondary" gutterBottom variant="h6">
            Loans Issued
          </Typography>
          <Typography className="chip-card" color="textPrimary" variant="h3">
            5,012
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
);

export default Budget;
