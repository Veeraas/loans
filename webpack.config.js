// const webpack = require("webpack");

module.exports = {
  devServer: {
    hot: true,
    open: true,
    port: 1780,
    compress: true,
    contentBase: "/www/",
    disableHostCheck: true,
    historyApiFallback: true,
    watchOptions: {
      poll: 1000,
    },
  },
};
